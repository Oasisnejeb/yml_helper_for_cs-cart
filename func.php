<?php

if ( !defined('AREA') ) { die('Access denied'); }

use Tygh\Registry;

function fn_yml_helper_update_product ($product_id,$values) {
	db_query("UPDATE ?:products SET ?u WHERE product_id = ?i", $values, $product_id);
}

function fn_yml_helper_check_products () {
	$start = microtime(true);
	$options = Registry::get('addons.yml_helper');
	$yml_pickup = $options['yml_helper_yml_pickup'];
	$yml_delivery = $options['yml_helper_yml_delivery'];
	$yml_store = $options['yml_helper_yml_store'];
	$disable_disabled = ($options['yml_helper_disable_disabled'] == 'Y')?true:false;
	$generate_delivery = ($options['yml_helper_generate_delivery_options'] == 'Y')?true:false;
	$add_avail = ($options['yml_helper_add_avail_since'] == 'Y')?true:false;
	$delivery_cost = (!empty($options['yml_helper_local_delivery_cost']))?intval($options['yml_helper_local_delivery_cost']):0;
	$delivery_period = (!empty($options['yml_helper_local_delivery_period']))?intval($options['yml_helper_local_delivery_period']):0;
	$delivery_time = (!empty($options['yml_helper_local_delivery_time']))?intval($options['yml_helper_local_delivery_time']):13;
	$product_exempts = explode(",", $options['yml_helper_product_regexps']);
	$check_products = (($options['yml_helper_check_products'] == 'Y')&&(!empty($product_exempts)))?true:false;
	$check_descriptions = (($options['yml_helper_check_descriptions'] == 'Y')&&(!empty($product_exempts)))?true:false;
	$features_exempts = explode(",", $options['yml_helper_features_regexps']);
	$check_features = (($options['yml_helper_check_features'] == 'Y')&&(!empty($features_exempts)))?true:false;
	$generate_notes = ($options['yml_helper_generate_yml_notes'] == 'Y')?true:false;
	$add_reward_points = ($options['yml_helper_add_reward_points'] == 'Y')?true:false;
	$notes_text_available = $options['yml_helper_yml_notes_text_available'];
	$notes_text_for_order = $options['yml_helper_yml_notes_text_for_order'];
	$analyze_images = ($options['yml_helper_analyze_images'] == 'Y')?true:false;
	$analyze_json_images = (($options['yml_helper_analyze_json_images'] == 'Y')&&(Registry::get('addons.exim_json.status') == 'A'))?true:false;
	$fields = array('p.product_id','p.amount','p.avail_since');
	if ($analyze_json_images) $fields[] = 'p.exim_json_images_q';
	if ($check_products) $fields[] = 'pd.product';
	if ($check_descriptions) $fields = array_merge($fields,array('pd.full_description','pd.short_description'));
	if ($generate_notes&&$add_reward_points) $fields[] = 'pp.price';
	$fields = implode(",", $fields);
	$allow_product = array(
		'yml_pickup' => $yml_pickup,
		'yml_delivery' => $yml_delivery,
		'yml_store' => $yml_store,
		'yml2_pickup' => $yml_pickup,
		'yml2_delivery' => $yml_delivery,
		'yml2_store' => $yml_store,
		'yml_export_yes' => 'Y',
		'yml2_export_yes' => 'Y'
		);
	$deny_product = array('yml_export_yes' => 'N', 'yml2_export_yes' => 'N');
	$bad_prod_ids = array();
	// check product feature variants that contain bad words from list
	if ($check_features) {
		$bad_variants = array();
		$features_where = "";
		if (!empty(YML_FEATURE_IDS)) {
			$features_where = " WHERE v.feature_id IN (" . implode(",", YML_FEATURE_IDS) . ")";
		}
		$all_variants = db_get_hash_array("SELECT v.variant_id, vd.variant FROM ?:product_feature_variants AS v 
		INNER JOIN ?:product_feature_variant_descriptions AS vd ON v.variant_id = vd.variant_id " . $features_where, 'variant_id');
		foreach ($all_variants as $variant_id => $options) {
			foreach ($features_exempts as $fexempt) {
				if (strripos($options['variant'], $fexempt) !== false) {
					$bad_variants[] = $variant_id;
					break;
				}
			}
		}

		if (!empty($bad_variants)) {
			$bad_prod_ids = db_get_fields("SELECT DISTINCT product_id FROM ?:product_features_values WHERE variant_id IN (" . implode(",", $bad_variants) . ")");
			if (!empty($bad_prod_ids)) {
				db_query("UPDATE ?:products SET ?u WHERE product_id IN (" . implode(",", $bad_prod_ids) . ")", $deny_product);
			}
		}
	}
	$now = time();
	$product_where = " WHERE status != ?s";
	$product_join = " INNER JOIN ?:product_descriptions as pd ON p.product_id = pd.product_id";
	if (!empty($bad_prod_ids)) {
		$product_where .= " AND p.product_id NOT IN (" . implode(",", $bad_prod_ids) . ")";
	}
	//modify SQL to add reward points to product_notes field
	if ($generate_notes&&$add_reward_points) {
		$product_join .= " LEFT JOIN ?:product_prices AS pp ON p.product_id = pp.product_id ";
		$product_where .= " AND pp.usergroup_id = " . strval(PRICE_USERGROUP_ID);
	}
	$products = db_get_hash_array("SELECT " . $fields . " FROM ?:products AS p" . $product_join . $product_where, 'product_id', 'D');
	foreach ($products as $product_id => $options) {
		if ((($options['amount'] < 1)&&(($avail_period = ($options['avail_since'] - $now)) < 0))||($analyze_json_images&&(!intval($options['exim_json_images_q'])))) {
			fn_yml_helper_update_product($product_id,$deny_product);
			continue;
		}
		// check products' names and descriptions for bad words and disable them if need
		if ($check_products||$check_descriptions) {
			$text = $options['product'] . $options['full_description'] . $options['short_description'];
			$bad_text = false;
			foreach ($product_exempts as $exempt) {
				if (strripos($text,$exempt) !== false) {
					$bad_text = true;
					break;
				}
			}
			if ($bad_text) {
				fn_yml_helper_update_product($product_id,$deny_product);
				continue;
			}
		}
		$product_data = array();
		if ($generate_notes) {
			//add reward points
			$product_data['yml2_sales_notes'] = ($options['amount'] > 0)?$notes_text_available:$notes_text_for_order;
			if ($add_reward_points&&$options['price']) $product_data['yml2_sales_notes'] .= "," . ceil($options['price']*REWARD_POINTS_COEF) . " руб на бонусный счет";
		}
		if ($generate_delivery) {
			// add delivery options
			$delivery = ($add_avail&&($options['amount'] < 1))?ceil($avail_period/86400)+$delivery_period:$delivery_period;
			$product_data['yml2_delivery_options'] = serialize(array(0 => array('cost' => $delivery_cost, 'days' => strval($delivery), 'order_before' => $delivery_time)));;
		}
		db_query("UPDATE ?:products SET ?u WHERE product_id = ?i", array_merge($allow_product,$product_data), $product_id);

	}
	if ($disable_disabled) db_query("UPDATE ?:products SET ?u WHERE status = ?s", $deny_product, 'D');
	fn_print_r('Время выполнения : '.(microtime(true) - $start).' сек.');
}

function fn_yml_helper_init_secure_controllers(&$controllers)
{
    $controllers['yml_helper'] = 'passive';
}

?>